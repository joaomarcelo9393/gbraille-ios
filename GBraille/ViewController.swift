//
//  ViewController.swift
//  GBraille
//
//  Created by João Marcelo on 14/10/15.
//  Copyright © 2015 Universidade Federal do Ceará. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
    }
}

